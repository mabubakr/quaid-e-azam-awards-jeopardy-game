package thegamestack.quaid.jeopardy.swing;

import javax.swing.JPanel;

/**
 * @author Mohammad Abubakr (mabubakr94@gmail.com)
 * @since 2016-08-31
 */
public class CategoriesPanel extends JPanel {

    private final GameController controller;

    public CategoriesPanel( GameController controller) {
        this.controller = controller;
        setupCategories();
    }

    private void setupCategories() {

    }
}
