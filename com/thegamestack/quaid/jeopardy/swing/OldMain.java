package thegamestack.quaid.jeopardy.swing;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class OldMain  {

    // TODO: REWRITE THIS ALL!

	private static final int CATEGORIES = 5;
    private static final int QUESTIONS = 5;

	private JPanel contentPane; // The content pane.

	// Objects used to run program in full screen.
	private GraphicsDevice device;
	private GraphicsEnvironment ge;

	private JLabel lblHeading = new JLabel(); // The title of the Jeopardy Game.
	private JLabel[] categories = new JLabel[CATEGORIES]; // Categories used in the game.
	private JLabel[][] question = new JLabel[CATEGORIES][QUESTIONS]; // Questions in the game
	private JLabel[][] answer = new JLabel[CATEGORIES][QUESTIONS]; // Answers to questions in the game

	private JButton[][] questionButtons = new JButton[CATEGORIES][QUESTIONS];


    Object[] options = {"Round 1", "Round 2"};
	InputStream is;
	BufferedReader in;

	int width, height;
	boolean isMain = true;

    private JFrame gameWindow;
    private JFrame controlWindow;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
            try {
                new OldMain();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
	}

	/**
	 * Create the frame.
	 * @throws java.io.IOException
	 */
	public OldMain() throws IOException {

        // Determine which round is being played.
        int choice = JOptionPane.showOptionDialog(gameWindow, "Which round is being played?", "Knowledge Contest Round", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
        if (choice == 0) {
            is =  getClass().getResourceAsStream("R1.txt");
        } else {
            is =  getClass().getResourceAsStream("R2.txt");
        }
        in = new BufferedReader(new InputStreamReader(is));

//        String[] players = JOptionPane.showInputDialog(null, "Enter the names of each player, separated by commas.",
//                                                       "Quaid-e-Azam Awards Knowledge Contest",
//                                                       JOptionPane.QUESTION_MESSAGE).split(",");

		ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        setupGameWindow();
//        setupControlWindow(players);


    }

    private void setupControlWindow( String[] players ) {
        controlWindow = new JFrame();
        JPanel controlPanel = new JPanel();


    }

    private void setupGameWindow() throws IOException {
        gameWindow = new JFrame();

        // Set look
        gameWindow.setTitle("Quaid-E-Azam Award Knowledge Contest");
        gameWindow.setVisible(true);
        gameWindow.setForeground(Color.GREEN);
        gameWindow.setBackground(Color.GREEN);
        gameWindow.setResizable(false);

        // Setup JFrame properties
        gameWindow.dispose();
        gameWindow.setUndecorated(true);
//		device.setFullScreenWindow(this);
        gameWindow.setVisible(true);
        gameWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Setup the content pane
        contentPane = new JPanel();
        contentPane.setBackground(new Color(34, 139, 34));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        gameWindow.setContentPane(contentPane);
        contentPane.setLayout(null);

        // Get the size of the screen and set size.
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        width = (int) dim.getWidth();
        height = (int) dim.getHeight();
        gameWindow.setSize(width, height);
//        gameWindow.setLocation(ge.getScreenDevices()[1].getDefaultConfiguration().getBounds().x, 0);

        setupView();
    }

    private void setupView() throws IOException {
		//Setup title text.
		lblHeading.setForeground(new Color(255, 255, 255));
		lblHeading.setFont(new Font("Tahoma", Font.BOLD, 40));
		lblHeading.setText(in.readLine());
		int strWidth = lblHeading.getFontMetrics(lblHeading.getFont()).stringWidth(lblHeading.getText());
		int strHeight = lblHeading.getFontMetrics(lblHeading.getFont()).getHeight();
		lblHeading.setBounds(width / 2 - (strWidth / 2), 11, strWidth, strHeight);
		contentPane.add(lblHeading);

		int catWidth = 0, catHeight = 0;
		//Setup category labels and questions/answers.
		for (int i = 0; i < CATEGORIES; i++){
			categories[i] = new JLabel();
			categories[i].setText("<html>" + in.readLine() + "</html>");
			categories[i].setFont(new Font("Tahoma", Font.BOLD, 35));
            categories[i].setForeground(new Color(255, 255, 255));
			catWidth = categories[i].getFontMetrics(categories[i].getFont()).stringWidth(categories[i].getText());
			catHeight = categories[i].getFontMetrics(categories[i].getFont()).getHeight();
			categories[i].setBounds((i * (width / CATEGORIES)) + width / 20, 81, catWidth, catHeight);
			contentPane.add(categories[i]);
            // For each category, set up its questions and answers.
			for (int j = 0; j < QUESTIONS; j++){
				final int temp1 = i, temp2 = j;
				String[] temp = in.readLine().split("\\*\\*");
				question[i][j] = new JLabel("<html>" + temp[0] + "</html>", JLabel.CENTER);
				question[i][j].setForeground(new Color(255, 255, 255));
				question[i][j].setFont(new Font("Tahoma", Font.BOLD,70));

                // Special cases for certain categories.
				if (categories[i].getText().contains("Who Am I?")){
					question[i][j].setFont(new Font("Tahoma", Font.BOLD,50));
				} else if (temp[0].contains(".jpg")) {
                    question[i][j].setIcon(new ImageIcon("com/thegamestack/quaid/jeopardy/swing/" + temp[0]));
                    question[i][j].setText("<html>                  </html>");
                }

				strWidth = question[i][j].getFontMetrics(question[i][j].getFont()).stringWidth(question[i][j].getText());
				strHeight = question[i][j].getFontMetrics(question[i][j].getFont()).getHeight();
				question[i][j].setBounds(width / 2 - (strWidth / 2), height / 2 - (strHeight * 15 / 2),
                        strWidth, strHeight * 15 );
				contentPane.add(question[i][j]);
				question[i][j].setVisible(false);
				question[i][j].addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0){
						question[temp1][temp2].setVisible(false);
						answer[temp1][temp2].setVisible(true);
					}
				});
				answer[i][j] = new JLabel("<html>" + temp[1] + "</html>");
				answer[i][j].setHorizontalAlignment(SwingConstants.CENTER);
				answer[i][j].setForeground(new Color(255, 255, 255));
				answer[i][j].setFont(new Font("Tahoma", Font.BOLD,70));
				strWidth = answer[i][j].getFontMetrics(answer[i][j].getFont()).stringWidth(answer[i][j].getText());
				strHeight = answer[i][j].getFontMetrics(answer[i][j].getFont()).getHeight();
				answer[i][j].setBounds(width / 2 - (strWidth / 2),
                        height / 2 - (strHeight * 10/ 2), strWidth, strHeight * 10);
				contentPane.add(answer[i][j]);
				answer[i][j].setVisible(false);
				answer[i][j].addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0){
						switchViews(temp1, temp2);
						answer[temp1][temp2].setVisible(false);
					}
				});
			}
		}

		//Setup all question buttons to show values from 100 - 500.
		for (int i = 0; i < CATEGORIES; i++){
			for (int z = 0; z < QUESTIONS; z++){
				questionButtons[i][z] = new JButton(String.valueOf((z + 1) * 100));
				questionButtons[i][z].setFont(new Font("Tahoma", Font.PLAIN, 16));
				questionButtons[i][z].setBounds(((i * (width / CATEGORIES)) + width / 20),
                        ((z * ((height - 70) / 6)) + (height - 70) / 15) + 111, width / 8, height / 10);
				contentPane.add(questionButtons[i][z]);
				final int temp1 = i, temp2 = z;
				questionButtons[i][z].addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0){
						if (questionButtons[temp1][temp2].isEnabled()){
						questionButtons[temp1][temp2].setEnabled(false);
						switchViews(temp1, temp2);
						}
					}
				});
//				System.out.println(questionButtons[i][z].getText());
			}
		}
	}



	private void switchViews(int temp1, int temp2) {
		for (int i = 0; i < CATEGORIES; i++){
			categories[i].setVisible(!isMain);
			for (int j = 0; j < QUESTIONS; j++){
				questionButtons[i][j].setVisible(!isMain);
			}
		}
		question[temp1][temp2].setVisible(isMain);
		isMain = !isMain;
	}
}
