package thegamestack.quaid.jeopardy.swing;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author Mohammad Abubakr (mabubakr94@gmail.com)
 * @since 2016-08-31
 */
public class GameController {

    private JFrame mainWindow;

    public GameController( JFrame mainWindow ) {
        this.mainWindow = mainWindow;
    }

    public void setContent( JPanel panel) {
        mainWindow.setContentPane(panel);
    }

    public void showQuestion( QuestionAnswerPanel questionAnswerPanel, String question, String answer ) {

    }
}
