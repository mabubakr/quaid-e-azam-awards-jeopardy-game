package thegamestack.quaid.jeopardy.swing;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Mohammad Abubakr (mabubakr94@gmail.com)
 * @since 2016-08-31
 */
public class Main {

    JFrame frame;

    GameController controller;

    JPanel              gamePanel;
    CategoriesPanel     categoriesPanel;
    QuestionAnswerPanel questionAnswerPanel;
    ScorePanel          scorePanel;

    public static void main( String[] args ) {
        new Main();
    }

    public Main() {
        String[] players = JOptionPane.showInputDialog(null, "Enter the names of each player, separated by commas.",
                                                       "Quaid-e-Azam Awards Knowledge Contest",
                                                       JOptionPane.QUESTION_MESSAGE).split(",");

        InputStream is;
        BufferedReader      in;
        Object[]        options = { "Round 1", "Round 2" };
        int choice = JOptionPane.showOptionDialog(null, "Which round is being played?", "Knowledge Contest Round",
                                                  JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                                                  options, options[0]);
        if (choice == 0) {
            is =  getClass().getResourceAsStream("R1.txt");
        } else {
            is =  getClass().getResourceAsStream("R2.txt");
        }
        in = new BufferedReader(new InputStreamReader(is));


        setupView(players, in);

    }

    private void setupView( String[] players, BufferedReader in ) {
        frame = new JFrame();

        controller = new GameController(frame);

        questionAnswerPanel = new QuestionAnswerPanel();

//        categoriesPanel = new CategoriesPanel(controller, questionAnswerPanel);

        scorePanel = new ScorePanel();

        gamePanel = new JPanel();
        gamePanel.setLayout(new BorderLayout());
        gamePanel.add(categoriesPanel, BorderLayout.CENTER);
        gamePanel.add(scorePanel, BorderLayout.SOUTH);


        frame.setContentPane(gamePanel);
        frame.setVisible(true);
    }
}
